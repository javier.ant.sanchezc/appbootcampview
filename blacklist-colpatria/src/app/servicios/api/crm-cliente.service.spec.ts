
import { of } from 'rxjs';
import { CrmClienteService } from './crm-cliente.service';


describe('#CrmClienteService', () => {
  let service: CrmClienteService;
  let httpClientSpy: { post: jasmine.Spy
    delete:jasmine.Spy }; //TODO: 🙄


  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['post','delete']);
    service = new CrmClienteService(httpClientSpy as any);
  });

  // TEST 1
  it('CRM Service existe', () => {
    expect(service).toBeTruthy();
  });

  /*
  // TEST 2
  it('getAll Clientes', (done: DoneFn) => {

    service.getAll().subscribe(clientes => {
      expect(clientes.data[0].length).toBe(7);
      done()
    });
  });*/

  //TEST 3
  it('Eliminar un cliente', (done: DoneFn) => {
    let responeMock ={
      "data": {
          "removed": true
      },
      "errores": null
      }
    httpClientSpy.delete.and.returnValue(of(responeMock)); // Remember to import {of} from 'rxjs'
    
     // service
     service.deleteCliente(80)
     .subscribe(resultado => {  
        expect(resultado).toEqual(responeMock)
        done()
      })
  });



});