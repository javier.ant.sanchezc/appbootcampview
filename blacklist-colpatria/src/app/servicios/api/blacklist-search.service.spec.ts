import { HttpErrorResponse } from '@angular/common/http';
import { of, throwError } from 'rxjs';
import { BlacklistSearchService } from './blacklist-search.service';

import {CustomerInfo} from '../../modelo/CustomerInfo';

describe('#BlacklistSearchService.search', () => {
  let service: BlacklistSearchService;
  let httpClientSpy: { post: jasmine.Spy }; //TODO: 🙄
   

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['post']);
    service = new BlacklistSearchService(httpClientSpy as any);
  });
 
  // TEST 1
  it('BlacklistSearchService existe', () => {
    expect(service).toBeTruthy();
  });

  // TEST 2
  it('Deberia retornar response customer (NO ESTA EN LA LISTA)', (done: DoneFn) => {

    // Mock de datos!
    const newCustomer= new CustomerInfo('string','string','string','string',
    new Date("2021-09-16"+'T00:00:00'),
    new Date("2021-09-16"+'T00:00:00'),'user@example.com','21312323');
  
    //Response
    const mockResult = {
        "data": {
            "blackListed": false,
            "document": "string"
        },
        "errores": null
    }

    httpClientSpy.post.and.returnValue(of(mockResult)) // Observable
    // service
    service.search(newCustomer)
      .subscribe(resultado => {  
        expect(resultado).toEqual(mockResult)
        done()
      })
  });

  
  // TEST 3
  it('Deberia retornar response customer (SI ES BLACKLIST)', (done: DoneFn) => {

    // Mock de datos!
    const newCustomer= new CustomerInfo('CC','10974003','Felipe','Buitrago',
    new Date("2011-09-02"+'T00:00:00'),
    new Date("1993-09-01"+'T00:00:00'),'afbuitragof@uq.com','3194962307');
  
    //Response
    const mockResult = {
        "data": {
            "blackListed": true,
            "document": "string"
        },
        "errores": null
    }

    httpClientSpy.post.and.returnValue(of(mockResult)) // Observable
    // service
    service.search(newCustomer)
      .subscribe(resultado => {  
        expect(resultado).toEqual(mockResult)
        done()
      })
  });



}); 