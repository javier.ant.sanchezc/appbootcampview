import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';

import {BlackListResponse} from '../../modelo/BlackListResponse';
import {Data} from '../../modelo/Data';
import {CustomerInfo} from '../../modelo/CustomerInfo';
import { UrlBlacklistSearch } from './enpointUrl'; 

@Injectable({
  providedIn: 'root'
})

export class BlacklistSearchService {

  private urlEndpoint = '';

  constructor(private http:HttpClient) {  
    this.urlEndpoint=UrlBlacklistSearch.url;
  }
 
  search(customerInfo:CustomerInfo): Observable<any> {
    let headers =new HttpHeaders({'Content-Type':'application/json'});
    return this.http.post<any>(this.urlEndpoint,customerInfo,{headers:headers});
  }


}
