import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs'; 
import { CustomerInfo } from 'src/app/modelo/CustomerInfo';

import { ListaCustomer } from 'src/app/modelo/ListaCustomer';

import { UrlCRMAll, UrlRegisterCRM,UrlDeleteCRM } from './enpointUrl'; 
 

@Injectable({
  providedIn: 'root'
})
export class CrmClienteService {

  private urlEndpointCRMAll = '';
  private urlEndpointRegisterCRM = '';
  private urlDeleteCRM = '';
  
  constructor(private http:HttpClient) {  
    this.urlEndpointCRMAll=UrlCRMAll.url;
    this.urlEndpointRegisterCRM=UrlRegisterCRM.url;
    this.urlDeleteCRM=UrlDeleteCRM.url;
  }
  
  getAll(): Observable<ListaCustomer>{  
    return this.http.get<ListaCustomer>(this.urlEndpointCRMAll);  
  }
 
  createCliente(customer:CustomerInfo): Observable<ListaCustomer>{  
    let headers =new HttpHeaders({'Content-Type':'application/json'});
    return this.http.post<any>(this.urlEndpointRegisterCRM,customer,{headers:headers});  
  }

  deleteCliente(id:number){  
    let headers =new HttpHeaders({'Content-Type':'application/json'});
    return this.http.delete<any>(this.urlDeleteCRM+'/'+id,{headers:headers});  
  }
  
}
