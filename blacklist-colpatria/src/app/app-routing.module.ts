import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlacklistFormularioComponent } from './blacklist-formulario/blacklist-formulario.component';
import { ClienteListComponent } from './cliente-list/cliente-list.component';
import { LoginComponent } from './login/login.component';
 
const routes: Routes = [ 
  {path:'login', component:LoginComponent},
 /* { path: '**', pathMatch: 'full', redirectTo: 'login' },
  { path: '', pathMatch: 'full', redirectTo: 'login' },*/
  {path:'blacklist-search', component:BlacklistFormularioComponent},
  {path:'cliente-lista', component:ClienteListComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }