
export class CustomerInfo {

    constructor(
        public documentType:      string,
        public document:          string,
        public name:              string,
        public lastName:          string,
        public documentIssueDate: Date,
        public birthDate:         Date,
        public email:             string,
        public cellphone:         string
    ){}
    
}