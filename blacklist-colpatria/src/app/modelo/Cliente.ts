
export class Cliente {

    constructor(
        public id:                number,
        public documentType:      string,
        public document:          string, 
        public fullName:          string,
        public documentIssueDate: Date,
        public birthDate:         Date,
        public email:             string,
        public cellphone:         string
    ){}
    
}