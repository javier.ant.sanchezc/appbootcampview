import { Component, OnInit } from '@angular/core'; 
import { PageEvent } from '@angular/material/paginator'; 
import { Router } from '@angular/router';
//servicios
import { CrmClienteService } from '../servicios/api/crm-cliente.service';
//modelos
import { Cliente } from '../modelo/Cliente';
import { ListaCustomer } from '../modelo/ListaCustomer';


@Component({
  selector: 'app-cliente-list',
  templateUrl: './cliente-list.component.html',
  styleUrls: ['./cliente-list.component.css']
})
export class ClienteListComponent implements OnInit {
 

  public arrayDatum = new Array<Cliente[]>(); 
  public listCustomer =new  ListaCustomer(this.arrayDatum,'');
  page_size:number=5;
  page_number:number=1;
  pageSizeOptions=[5,10,20,50,100];
  tamanoData:number=0;


  constructor(private _serviceCRM:CrmClienteService, private _router:Router) {
   }

  ngOnInit(): void {
    this.getClientes(); 
  }

  handlePage(e:PageEvent){
    this.page_size=e.pageSize;
    this.page_number=e.pageIndex+1;
  }

  getClientes(){
    this.setDelay();
    this._serviceCRM.getAll().subscribe(
      (result:ListaCustomer)=>{      
        this.listCustomer=result;
        console.log(this.listCustomer); 
        console.log(this.listCustomer.data[0]);
        this.tamanoData=this.listCustomer.data[0].length;
      }
    ); 
  }

  setDelay() {
    setTimeout(function(){
      console.log('time out');
    }, 900000);
  }

  eliminarCliente(id:number){
    console.log('id borrar:'+id);   
    this._serviceCRM.deleteCliente(id).subscribe(
      respuesta=>
      {console.log(respuesta)
        //this._router.navigate(['/home']);
        window.location.reload();
      }
    );
  }

}
