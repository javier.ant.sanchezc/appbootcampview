import { Component, OnInit } from '@angular/core'; 

import {BlackListResponse} from '../modelo/BlackListResponse';
import {Data} from '../modelo/Data';
import {CustomerInfo} from '../modelo/CustomerInfo';

import { BlacklistSearchService } from '../servicios/api/blacklist-search.service'; 
import { CrmClienteService } from '../servicios/api/crm-cliente.service';



@Component({
  selector: 'app-blacklist-formulario',
  templateUrl: './blacklist-formulario.component.html',
  styleUrls: ['./blacklist-formulario.component.css']
})
export class BlacklistFormularioComponent implements OnInit {

  public customerInfo: CustomerInfo;
  public dataResp=new Data(false,'');
  public blackListResponse=new BlackListResponse(this.dataResp,'');
  public verModalValidacion:boolean=false;
  public verModal:boolean=false;
  public birthDateString:string='';
  public documentIssueDateString:string=''; 
  public mensajeCuardadoCliente:string='';

  constructor(private _blacklistSearchService:BlacklistSearchService, private  _crmClienteService:CrmClienteService) {                    
    this.customerInfo= new CustomerInfo('','','','',new Date(),new Date(),'','');    
  }

  ngOnInit(): void {   
  }

  onSubmit(validacionForm:boolean){
    this.verModal=true;
    this.verModalValidacion=validacionForm;

    if(!validacionForm){
      this.customerInfo.birthDate = new Date(this.birthDateString+'T00:00:00');
      this.customerInfo.documentIssueDate = new Date(this.documentIssueDateString+'T00:00:00');
      console.log(this.customerInfo);
      this._blacklistSearchService.search(this.customerInfo)
      .subscribe(response=>{   
        this.blackListResponse=response;
        console.log(this.blackListResponse);           
        }
      );
    }        
  }

  registrarCliente(customerInfo:CustomerInfo){
    console.log('cliente');
    console.log(customerInfo);
    this._crmClienteService.createCliente(customerInfo)
      .subscribe(
        resp=>
        {
          console.log(resp);
        }
    );    
    this.cerrarModal(true);
  }

  cerrarModal(borrarForm:boolean){
    this.verModalValidacion=false;
    this.verModal=false; 
    this.mensajeCuardadoCliente='';
    if(borrarForm){
      this.customerInfo= new CustomerInfo('','','','',new Date(),new Date(),'',''); 
      this.birthDateString='';
      this.documentIssueDateString=''; 
      this.mensajeCuardadoCliente='El cliente se guardo satisfactoriamente';
    }
    
  }



}
